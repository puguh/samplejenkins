package com.puguh.asiafintektest.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "TM_EMPLOYEE")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nik;
    private String name;
    private String departement;
    private String position;
}
