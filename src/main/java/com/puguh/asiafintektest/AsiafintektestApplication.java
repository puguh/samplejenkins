package com.puguh.asiafintektest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AsiafintektestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AsiafintektestApplication.class, args);
	}

}
