package com.puguh.asiafintektest.controller;


import com.puguh.asiafintektest.model.Employee;
import com.puguh.asiafintektest.repo.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class EmployeeController {

    @Autowired
    EmployeeRepo employeeRepo;

    @Value("${welcome.message}")
    private String message;

    private List<String> tasks = Arrays.asList("a", "b", "c", "d", "e", "f", "g");

    @GetMapping("/")
    public String main(Model model) {
        model.addAttribute("message", message);
        model.addAttribute("tasks", tasks);
        //employeeRepo.findAll();

        model.addAttribute("employees",employeeRepo.findAll());
        return "employee"; //view
    }

    @GetMapping("/addEmployee")
    public String add(Model model) {
            model.addAttribute("employee", new Employee());
        return "addEmployee"; //view
    }

    @PostMapping("/saveEmployee")
    public String saveEmployee(Model model, Employee employee) {

        employeeRepo.save(employee);
        model.addAttribute("employees", employeeRepo.findAll());
        return "employee";
    }

    @PostMapping("/listEmployee")
    public @ResponseBody Iterable<Employee>  listEmployee() {
        List<Employee> customers = new ArrayList<>();

        return employeeRepo.findAll();
    }
}
