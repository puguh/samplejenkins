package com.puguh.asiafintektest.repo;

import com.puguh.asiafintektest.model.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepo extends CrudRepository<Employee, Long> {
}
